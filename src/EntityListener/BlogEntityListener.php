<?php

namespace App\EntityListener;

use App\Entity\Blog;
use Symfony\Component\String\Slugger\SluggerInterface;

class BlogEntityListener
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Blog $conference)
    {
        $conference->computeSlug($this->slugger);
    }

    public function preUpdate(Blog $conference)
    {
        $conference->computeSlug($this->slugger);
    }
}